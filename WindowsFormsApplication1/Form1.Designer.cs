﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dataGridView_Flights = new System.Windows.Forms.DataGridView();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.button_pokaz = new MaterialSkin.Controls.MaterialRaisedButton();
            this.comboBox_city_start = new System.Windows.Forms.ComboBox();
            this.comboBox_city_end = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Flights)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView_Flights
            // 
            this.dataGridView_Flights.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Flights.Location = new System.Drawing.Point(303, 119);
            this.dataGridView_Flights.Name = "dataGridView_Flights";
            this.dataGridView_Flights.RowTemplate.Height = 28;
            this.dataGridView_Flights.Size = new System.Drawing.Size(372, 354);
            this.dataGridView_Flights.TabIndex = 6;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(38, 156);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(236, 27);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "LOTNISKO STARTOWE";
            this.materialLabel1.Click += new System.EventHandler(this.materialLabel1_Click);
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(37, 279);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(237, 27);
            this.materialLabel2.TabIndex = 10;
            this.materialLabel2.Text = "LOTNISKO DOCELOWE";
            // 
            // button_pokaz
            // 
            this.button_pokaz.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_pokaz.Depth = 0;
            this.button_pokaz.Location = new System.Drawing.Point(42, 382);
            this.button_pokaz.MouseState = MaterialSkin.MouseState.HOVER;
            this.button_pokaz.Name = "button_pokaz";
            this.button_pokaz.Primary = true;
            this.button_pokaz.Size = new System.Drawing.Size(231, 91);
            this.button_pokaz.TabIndex = 13;
            this.button_pokaz.Text = "OBLICZ TRASĘ";
            this.button_pokaz.UseVisualStyleBackColor = true;
            this.button_pokaz.Click += new System.EventHandler(this.button_pokaz_Click);
            // 
            // comboBox_city_start
            // 
            this.comboBox_city_start.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBox_city_start.FormattingEnabled = true;
            this.comboBox_city_start.Items.AddRange(new object[] {
            "Wroclaw",
            "Warszawa",
            "Berlin",
            "Londyn",
            "Frankfurt",
            "Rzym"});
            this.comboBox_city_start.Location = new System.Drawing.Point(42, 196);
            this.comboBox_city_start.Name = "comboBox_city_start";
            this.comboBox_city_start.Size = new System.Drawing.Size(232, 28);
            this.comboBox_city_start.TabIndex = 16;
            this.comboBox_city_start.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox_city_end
            // 
            this.comboBox_city_end.Cursor = System.Windows.Forms.Cursors.Hand;
            this.comboBox_city_end.FormattingEnabled = true;
            this.comboBox_city_end.Items.AddRange(new object[] {
            "Wroclaw",
            "Warszawa",
            "Berlin",
            "Londyn",
            "Frankfurt",
            "Rzym"});
            this.comboBox_city_end.Location = new System.Drawing.Point(42, 323);
            this.comboBox_city_end.Name = "comboBox_city_end";
            this.comboBox_city_end.Size = new System.Drawing.Size(232, 28);
            this.comboBox_city_end.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(686, 539);
            this.Controls.Add(this.comboBox_city_end);
            this.Controls.Add(this.comboBox_city_start);
            this.Controls.Add(this.button_pokaz);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.dataGridView_Flights);
            this.Name = "Form1";
            this.Text = "APLIKACJA - WYZNACZANIE TRAS LOTNICZYCH";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Flights)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView_Flights;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialRaisedButton button_pokaz;
        private System.Windows.Forms.ComboBox comboBox_city_start;
        private System.Windows.Forms.ComboBox comboBox_city_end;
    }
}

