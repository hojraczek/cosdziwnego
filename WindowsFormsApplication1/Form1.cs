﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{

    public partial class Form1 : MaterialSkin.Controls.MaterialForm
    {

        int dlugosc = 0;

        public Form1()
        {
            InitializeComponent();

        }
        private List<String> list = new List<String>();
        //String city1;
        //String city2;


        private void button_pokaz_Click(object sender, EventArgs e)
        {

            Form1 g = new Form1();

            g.add_vertex("Wroclaw", new Dictionary<string, int>() { { "Warszawa", 60 }, { "Berlin", 45 }, { "Frankfurt", 80 } });
            g.add_vertex("Warszawa", new Dictionary<string, int>() { { "Frankfurt", 120 }, { "Berlin", 90 }, { "Wroclaw", 60 }, { "Warszawa", 190 } });
            g.add_vertex("Frankfurt", new Dictionary<string, int>() { { "Londyn", 140 }, { "Wroclaw", 80 }, { "Rzym", 130 }, { "Berlin", 60 }, { "Warszawa", 120 } });
            g.add_vertex("Londyn", new Dictionary<string, int>() { { "Frankfurt", 140 }, { "Rzym", 180 } });
            g.add_vertex("Rzym", new Dictionary<string, int>() { { "Berlin", 170 }, { "Warszawa", 190 }, { "Frankfurt", 130 }, { "Londyn", 180 } });
            g.add_vertex("Berlin", new Dictionary<string, int>() { { "Rzym", 170 }, { "Wroclaw", 45 }, { "Frankfurt", 60 }, { "Warszawa", 90 } });

            string MiastoA = comboBox_city_start.Text;
            string MiastoB = comboBox_city_end.Text;

            list = g.shortest_path(MiastoA, MiastoB);
            int lmiast = list.Count;
            //MessageBox.Show("Trasa:\n");
            //MessageBox.Show("start " + MiastoA);
            //wyswietlenie w tabeli wartosci
            DataTable dt = new DataTable();
            dt.Columns.Add("Skad");
            dt.Columns.Add("Dokad");
            dt.Columns.Add("Czas Podrozy");

            //dt.Rows.Add(new object[] { 1, MiastoA });

            string first = MiastoA;
            int dlugosc = 0;

            List<string> b = g.shortest_path(MiastoA, MiastoB);
                for (int i = b.Count - 1; i >= 0; i--)
                {
                    dt.Rows.Add(new object[] { first, b[i], g.vertices[b[i]][first] });
                    // Console.WriteLine(first + " " + b[i] + "weight: " + g.vertices[b[i]][first]);
                    dlugosc = dlugosc + g.vertices[b[i]][first];
                    first = b[i];
                }

            // for (int i = 0; i < lmiast; i++)
            //{
            //MessageBox.Show("numer: " + i + " - " + list[lmiast-1-i]);
            //   dt.Rows.Add(new object[] { i + 2, list[lmiast - 1 - i] });
            //}

            //city1 = list[0];
            //city2 = list[1];
            //g.shortest_path(MiastoA, MiastoB).ForEach(x => Console.WriteLine(x));


            dataGridView_Flights.DataSource = dt;
        }

        private object shortest_path(Func<char[]> toCharArray1, Func<char[]> toCharArray2)
        {
            throw new NotImplementedException();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {


        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        Dictionary<string, Dictionary<string, int>> vertices = new Dictionary<string, Dictionary<string, int>>();

        public void add_vertex(string name, Dictionary<string, int> edges)
        {
            vertices[name] = edges;
        }

        public List<string> shortest_path(string start, string finish)
        {
            var previous = new Dictionary<string, string>();
            var distances = new Dictionary<string, int>();
            var nodes = new List<string>();

            List<string> path = null;

            foreach (var vertex in vertices)
            {
                if (vertex.Key == start)
                {
                    distances[vertex.Key] = 0;
                }
                else
                {
                    distances[vertex.Key] = int.MaxValue;
                }

                nodes.Add(vertex.Key);
            }
            //int dlugosc = 0;
            while (nodes.Count != 0)
            {
                nodes.Sort((x, y) => distances[x] - distances[y]);

                var smallest = nodes[0];
                nodes.Remove(smallest);

                if (smallest == finish)
                {
                    path = new List<string>();
                    while (previous.ContainsKey(smallest))
                    {
                        path.Add(smallest);
                        smallest = previous[smallest];
                    }

                    break;
                }

                if (distances[smallest] == int.MaxValue)
                {
                    break;
                }

                foreach (var neighbor in vertices[smallest])
                {
                    var alt = distances[smallest] + neighbor.Value;
                    if (alt < distances[neighbor.Key])
                    {
                        distances[neighbor.Key] = alt;
                        previous[neighbor.Key] = smallest;
                    }

                    dlugosc = alt;
                }
            }
           // string nowa_dlugos = dlugosc.ToString();
            //textBox4.Text = nowa_dlugos;
            //dt.Rows.Add(new object[] { "Dlugosc", nowa_dlugos });
           // MessageBox.Show("Łączny czas podróży: " + dlugosc + " min");
            return path;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void materialLabel1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        //private void button_pokaz_Click(object sender, EventArgs e)
        //{


        //    Form1 g = new Form1();

        //    g.add_vertex("Wroclaw", new Dictionary<string, int>() { { "Warszawa", 60 }, { "Berlin", 45 }, { "Frankfurt", 80 } });
        //    g.add_vertex("Warszawa", new Dictionary<string, int>() { { "Frankfurt", 120 }, { "Berlin", 90 }, { "Wroclaw", 60 }, { "Rzym", 190 } });
        //    g.add_vertex("Frankfurt", new Dictionary<string, int>() { { "Londyn", 140 }, { "Wroclaw", 80 }, { "Rzym", 130 } });
        //    g.add_vertex("Londyn", new Dictionary<string, int>() { { "Frankfurt", 140 }, { "Rzym", 180 } });
        //    g.add_vertex("Rzym", new Dictionary<string, int>() { { "Berlin", 170 }, { "Warszawa", 190 }, { "Frankfurt", 130 }, { "Londyn", 180 } });
        //    g.add_vertex("Berlin", new Dictionary<string, int>() { { "Rzym", 170 }, { "Wroclaw", 45 } });

        //    string MiastoA = textBox1.Text;
        //    string MiastoB = textBox2.Text;

        //    list = g.shortest_path(MiastoA, MiastoB);
        //    int lmiast = list.Count;
        //    //MessageBox.Show("Trasa:\n");
        //    //MessageBox.Show("start " + MiastoA);
        //    //wyswietlenie w tabeli wartosci
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("Numer miasta");
        //    dt.Columns.Add("Miasto");

        //    dt.Rows.Add(new object[] { 1, MiastoA });

        //    for (int i = 0; i < lmiast; i++)
        //    {
        //        //MessageBox.Show("numer: " + i + " - " + list[lmiast-1-i]);
        //        dt.Rows.Add(new object[] { i + 2, list[lmiast - 1 - i] });
        //    }
            
        //    city1 = list[0];
        //    city2 = list[1];
        //    //g.shortest_path(MiastoA, MiastoB).ForEach(x => Console.WriteLine(x));


        //    dataGridView_Flights.DataSource = dt;
        //}

        private void textBox_czas_lotu_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
